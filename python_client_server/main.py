import sys
from http.server import BaseHTTPRequestHandler, HTTPServer
import http.client

class WebServer(BaseHTTPRequestHandler):    
    def do_POST(self):
        content_len = int(self.headers.get("Content-Length",0))
        post_body = self.rfile.read(content_len)
        print(f"client: {self.client_address} writes: {post_body.decode()}")
        self.send_response(200)
        self.send_header('Content-type', 'text')
        self.end_headers()
        self.wfile.write(b"Message delivered!")

def main():
    command = sys.argv
    command.pop(0) # to delete script name
    if len(command) == 1:
        print("Server mode")
        server = HTTPServer(('',int(command[0])), WebServer)
        server.serve_forever()
    if len(command) == 2:
        print("Client mode")
        while True:
            message = input("message: ")
            conn = http.client.HTTPConnection(host=command[0],port=int(command[1]))
            conn.request("POST", "/", body=message)
            response = conn.getresponse()
            print(response.read().decode('UTF-8'))
            


if __name__ == "__main__":
    main()